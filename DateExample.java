import java.sql.Date;
import java.time.LocalDate;
public class DateExample {
    public static void main(String[] args) {
        // Create a new Date object
        Date currentDate = new Date(System.currentTimeMillis());
        Date sqlDate = Date.valueOf(LocalDate.now());
        LocalDate localDate = sqlDate.toLocalDate();
        System.out.println("Date in yyyy-mm-dd format: " + localDate.toString());

        Date sqlDate2 = Date.valueOf("2022-12-31");
        System.out.println("Date in yyyy-mm-dd format: " + sqlDate2.toString());

        // Print the current date
        System.out.println("Current Date: " + currentDate);
        
        // Get the year, month, and day of the month from the Date object
        int year = currentDate.getYear() + 1900;
        int month = currentDate.getMonth() + 1;
        int day = currentDate.getDate();
        
        // Print the year, month, and day of the month
        System.out.println("Year: " + year);
        System.out.println("Month: " + month);
        System.out.println("Day of the Month: " + day);
    }
}
