import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.ArrayList;
import java.util.List;
class Ars extends JFrame{
	JButton b1,b2;
	Ars(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Traffic Accident Report System");
		b1 = new JButton("Add Report");
		b1.setBounds(80,45,120,30);
		add(b1);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
				Id id = new Id();
		        id.setBounds(400,200,400,400);
				id.setVisible(true);	
			}
		});
		b2 = new JButton("Report History");
		b2.setBounds(80,100,120,30);
		add(b2);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
				History h = new History();
				h.setBounds(40,50,1300,600);
				h.setVisible(true);
			}
		});
	}
}
class Id extends JFrame{   
	JTextField t1,t2;
	JButton b1,b2,b3;
	JLabel l1,l3,l4;
    Id(){
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        l1 = new JLabel("LOGIN");
		l1.setFont(new Font("Times new Roman",Font.BOLD,25));
		l1.setForeground(Color.red);
		l1.setBounds(100,10,300,30);
		add(l1);
        t1 = new JTextField(60);
		t2 = new JPasswordField(60);
		b1 = new JButton("SignIn");
		b2 = new JButton("SignUp");
		t1.setBounds(100,60,120,30);
		t2.setBounds(100,100,120,30);
		b1.setBounds(120,140,80,30);
		b2.setBounds(120,170,80,30);
        b3 = new JButton("<--Back");
        b3.setBounds(10,30,80,30);
		l3 = new JLabel("ID");
		l3.setBounds(20,60,50,30);
		add(l3);
		l4 = new JLabel("Password");
		l4.setBounds(20,100,70,30);
		add(l4);
		add(t1);
		add(t2);
		add(b1);
		add(b2);
        add(b3);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				boolean match = false;
				String uname = t1.getText().toString();
				String pwd = t2.getText().toString();
				try{
					FileReader fr = new FileReader("employeelogin.txt");
					BufferedReader br = new BufferedReader(fr);
					String str;
					while((str=br.readLine())!=null){
						if(str.equals(uname+"\t"+pwd)){
							match = true;
							break;
						}
					}
					fr.close();
				}catch(Exception e){}
				if(match){
					dispose();
					Addreport ar = new Addreport();
					ar.setBounds(400,60,250,250);
					ar.setVisible(true);
				}
				else{
					JOptionPane.showMessageDialog(null,"Invalid Username or Password!!","ERROR",JOptionPane.ERROR_MESSAGE);
				}
			}
				
		});
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				SigUp s = new SigUp();
				s.setVisible(true);
				s.setBounds(200,200,500,400);
			}
		});
        b3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
				Ars a = new Ars();
				a.setBounds(400,60,350,200);
				a.setVisible(true);
			}
		});
		
	}
}
class SigUp extends JFrame{
	JTextField t1,t2,t3,t4;
	JLabel l3,l4,l1,l2,l5;
	JButton b1,b2;
	SigUp(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		l1 = new JLabel("SignUp");
		l1.setFont(new Font("Times new Roman",Font.BOLD,20));
		l1.setForeground(Color.BLUE);
		l1.setBounds(40,10,100,30);
		add(l1);
		t1 = new JTextField(60);
		t2 = new JTextField(60);
		t3 = new JTextField(60);
		t4 = new JTextField(60);
		b1 = new JButton("Submit");
		b2 = new JButton("Cancel");
		t1.setBounds(100,50,80,30);
		t2.setBounds(100,90,80,30);
		t3.setBounds(100,130,80,30);
		t4.setBounds(100,170,150,30);
		l2 = new JLabel("Mobile No.");
		l2.setBounds(20,130,80,30);
		add(l2);
		l5 = new JLabel("Name");
		l5.setBounds(20,170,70,30);
		add(l5);
		l3 = new JLabel("ID");
		l3.setBounds(20,50,70,30);
		add(l3);
		l4 = new JLabel("Password");
		l4.setBounds(20,90,70,30);
		add(l4);
		b1.setBounds(100,210,80,30);
		b2.setBounds(100,250,80,30);
		add(t1);
		add(t2);
		add(t3);
		add(t4);
		add(b1);
		add(b2);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				if(t1.getText().toString().isEmpty()||t2.getText().toString().isEmpty()||t3.getText().toString().isEmpty()||t4.getText().toString().isEmpty()){
					JOptionPane.showMessageDialog(null,"Please Fill all the Fields","ERROR",JOptionPane.ERROR_MESSAGE);
				}else{
					try{
					FileWriter fw = new FileWriter("employeelogin.txt",true);
					fw.write(t1.getText()+"\t"+t2.getText()+"\n");
					fw.close();
					FileWriter fw1 = new FileWriter("employeedetails.txt",true);
					fw1.write(t1.getText()+","+t3.getText()+","+t4.getText()+"\n");
					fw1.close();
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f,"Registration Completed");
					dispose();
				}catch(Exception e){}
			}
		}
		});
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
			}
		});
	}
}
class Addreport extends JFrame{
	JButton b1,b2;
	JLabel l1,l2,l3;
	JTextField t1,t2,t3;
	Addreport(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Viewer Details");
		l1 = new JLabel("Vi_Name");
		l2 = new JLabel("Vi_Mobile No");
		l3 = new JLabel("Vi_Address");
		l1.setBounds(20,40,100,30);
		l2.setBounds(20,80,100,30);
		l3.setBounds(20,120,100,30);
		add(l1);
		add(l2);
		add(l3);
		t1 = new JTextField(30);
		t2 = new JTextField(30);
		t3 = new JTextField(30);
		t1.setBounds(130,40,100,30);
		t2.setBounds(130,80,100,30);
		t3.setBounds(130,120,100,30);
		add(t1);
		add(t2);
		add(t3);
		b1 = new JButton("<--Back");
		b1.setBounds(5,10,80,20);
		add(b1);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
				Id id = new Id();
				id.setBounds(400,60,400,400);
				id.setVisible(true);				
			}
		});
		b2 = new JButton("Save & Add Accident Location");
		b2.setBounds(10,160,220,30);
		add(b2);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(t1.getText().toString().isEmpty()||t2.getText().toString().isEmpty()||t3.getText().toString().isEmpty()){
					JOptionPane.showMessageDialog(null,"Please Fill all the Fields","ERROR",JOptionPane.ERROR_MESSAGE);
				}else{
				try{
					FileWriter fw1 = new FileWriter("alldetails.txt",true);
					fw1.write(t1.getText()+","+t2.getText()+","+t3.getText()+"\n");
					fw1.close();
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f,"Saved Successfully!!");
					dispose();
				}catch(Exception ex){}
				dispose();
				Location l = new Location();
				l.setBounds(300,60,300,300);
				l.setVisible(true);}
			}
		});
	}
}
class Location extends JFrame{
	JButton b2;
	JLabel l1,l2,l3,l4;
	JTextField t1,t2,t3,t4;
	Location(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Accident Location");
		l1 = new JLabel("S_Name");
		l2 = new JLabel("S_Number");
		l3 = new JLabel("S_Address");
		l4 = new JLabel("Accident Date");
		l1.setBounds(20,40,120,30);
		l2.setBounds(20,80,120,30);
		l3.setBounds(20,120,120,30);
		l4.setBounds(20,160,120,30);
		add(l1);
		add(l2);
		add(l3);
		add(l4);
		t1 = new JTextField(30);
		t2 = new JTextField(30);
		t3 = new JTextField(30);
		t4 = new JTextField(30);
		t1.setBounds(150,40,100,30);
		t2.setBounds(150,80,100,30);
		t3.setBounds(150,120,100,30);
		t4.setBounds(150,160,100,30);
		add(t1);
		add(t2);
		add(t3);
		add(t4);
		b2 = new JButton("Save & Add Vehicle Details");
		b2.setBounds(30,200,220,30);
		add(b2);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(t1.getText().toString().isEmpty()||t2.getText().toString().isEmpty()||t3.getText().toString().isEmpty()||t4.getText().toString().isEmpty()){
					JOptionPane.showMessageDialog(null,"Please Fill all the Fields","ERROR",JOptionPane.ERROR_MESSAGE);
				}else{
				try{
					FileWriter fw1 = new FileWriter("alldetails.txt",true);
					fw1.write(t1.getText()+","+t2.getText()+","+t3.getText()+","+t4.getText()+"\n");
					fw1.close();
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f,"Saved Successfully!!");
					dispose();
				}catch(Exception ex){}
				dispose();
				Vehicle l = new Vehicle();
				l.setBounds(300,60,300,400);
				l.setVisible(true);}
			}
		});
	}
}
class Vehicle extends JFrame{
	JButton b1,b2,b3;
	JLabel l1,l2,l3,l4,l5;
	JTextField t1,t2,t3,t4;
	JComboBox<String> c1;
	Vehicle(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Vehicle Details");
		l1 = new JLabel("V_Owner Name");
		l2 = new JLabel("V_Name");
		l3 = new JLabel("V_RegdNo");
		l4 = new JLabel("V_Regd_Date");
		l5 = new JLabel("V_Insurence ?");
		l1.setBounds(20,40,120,30);
		l2.setBounds(20,80,120,30);
		l3.setBounds(20,120,120,30);
		l4.setBounds(20,160,120,30);
		l5.setBounds(20,200,120,30);
		add(l1);
		add(l2);
		add(l3);
		add(l4);
		add(l5);
		t1 = new JTextField(30);
		t2 = new JTextField(30);
		t3 = new JTextField(30);
		t4 = new JTextField(30);
		t1.setBounds(150,40,100,30);
		t2.setBounds(150,80,100,30);
		t3.setBounds(150,120,100,30);
		t4.setBounds(150,160,100,30);
		add(t1);
		add(t2);
		add(t3);
		add(t4);
		c1 = new JComboBox<String>(new String[] {"Yes", "No"});
		c1.setBounds(150,200,70,30);
		add(c1);
		b1 = new JButton("Ok");
		b1.setBounds(220,200,50,30);
		add(b1);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String sl = (String) c1.getSelectedItem();
				if(sl.equals("Yes")){
					b3.setEnabled(true);
					b2.setEnabled(false);
				}
				else if(sl.equals("No")){
					b3.setEnabled(false);
					b2.setEnabled(true);
				}
				else{b3.setEnabled(false);
				b2.setEnabled(false);}
			}
		});
		b3 = new JButton("Add Insurence Details");
		b3.setBounds(30,240,220,30);
		b3.setEnabled(false);
		add(b3);
		b3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String sl = (String) c1.getSelectedItem();
				if(t1.getText().toString().isEmpty()||t2.getText().toString().isEmpty()||t3.getText().toString().isEmpty()||t4.getText().toString().isEmpty()||sl.isEmpty()){
					JOptionPane.showMessageDialog(null,"Please Fill all the Fields","ERROR",JOptionPane.ERROR_MESSAGE);
				}else{
				try{
					FileWriter fw1 = new FileWriter("alldetails.txt",true);
					fw1.write(t1.getText()+","+t2.getText()+","+t3.getText()+","+t4.getText()+","+"Yes"+"\n");
					fw1.close();
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f,"Saved Successfully!!");
					dispose();
				}catch(Exception ex){}
				dispose();
				Insurence ins = new Insurence(t3.getText());
				ins.setBounds(600,60,300,300);
				ins.setVisible(true);}
			}
		});
		b2 = new JButton("Save & Add Traffic Officer");
		b2.setBounds(30,280,220,30);
		add(b2);
		b2.setEnabled(false);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String sl = (String) c1.getSelectedItem();
				if(t1.getText().toString().isEmpty()||t2.getText().toString().isEmpty()||t3.getText().toString().isEmpty()||t4.getText().toString().isEmpty()||sl.isEmpty()){
					JOptionPane.showMessageDialog(null,"Please Fill all the Fields","ERROR",JOptionPane.ERROR_MESSAGE);
				}else{
				try{
					FileWriter fw1 = new FileWriter("alldetails.txt",true);
					fw1.write(t1.getText()+","+t2.getText()+","+t3.getText()+","+t4.getText()+","+"No"+"\n");
					fw1.close();
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f,"Saved Successfully!!");
					dispose();
				}catch(Exception ex){}
				dispose();
				Traffic to = new Traffic();
				to.setBounds(600,60,300,300);
				to.setVisible(true);}
			}
		});
	}
}
class Insurence extends JFrame{
	JLabel l1,l2,l3,l4;
	JTextField t1,t2,t3,t4;
	JButton b1,b2;
	Insurence(String vno){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Insurence Details");
		l1 = new JLabel("IC_Name");
		l2 = new JLabel("IC_Address");
		l3 = new JLabel("Policy_No");
		l4 = new JLabel("IC_Date");
		l1.setBounds(20,40,100,30);
		l2.setBounds(20,80,100,30);
		l3.setBounds(20,120,100,30);
		l4.setBounds(20,160,100,30);
		add(l1);
		add(l2);
		add(l3);
		add(l4);
		t1 = new JTextField(30);
		t2 = new JTextField(30);
		t3 = new JTextField(30);
		t4 = new JTextField(30);
		t1.setBounds(130,40,100,30);
		t2.setBounds(130,80,100,30);
		t3.setBounds(130,120,100,30);
		t4.setBounds(130,160,100,30);
		add(t1);
		add(t2);
		add(t3);
		add(t4);
		b2 = new JButton("Save & Add Traffic Officer");
		b2.setBounds(30,200,220,30);
		add(b2);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(t1.getText().toString().isEmpty()||t2.getText().toString().isEmpty()||t3.getText().toString().isEmpty()||t4.getText().toString().isEmpty()){
					JOptionPane.showMessageDialog(null,"Please Fill all the Fields","ERROR",JOptionPane.ERROR_MESSAGE);
				}else{
				try{
					FileWriter fw1 = new FileWriter("Insurencedetails.txt",true);
					fw1.write(vno+","+t1.getText()+","+t2.getText()+","+t3.getText()+","+t4.getText()+"\n");
					fw1.close();
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f,"Saved Successfully!!");
					dispose();
				}catch(Exception ex){}
				dispose();
				Traffic to = new Traffic();
				to.setBounds(600,60,300,300);
				to.setVisible(true);}
			}
		});
	}
}
class Traffic extends JFrame{
	JLabel l1,l2,l3,l4;
	JTextField t1,t2,t3,t4;
	JButton b2;
	Traffic(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("P_Officer Details");
		l1 = new JLabel("P_Name");
		l2 = new JLabel("P_ID");
		l3 = new JLabel("P_Mobile No");
		l4 = new JLabel("P_Location");
		l1.setBounds(20,40,100,30);
		l2.setBounds(20,80,100,30);
		l3.setBounds(20,120,100,30);
		l4.setBounds(20,160,100,30);
		add(l1);
		add(l2);
		add(l3);
		add(l4);
		t1 = new JTextField(30);
		t2 = new JTextField(30);
		t3 = new JTextField(30);
		t4 = new JTextField(30);
		t1.setBounds(130,40,100,30);
		t2.setBounds(130,80,100,30);
		t3.setBounds(130,120,100,30);
		t4.setBounds(130,160,100,30);
		add(t1);
		add(t2);
		add(t3);
		add(t4);
		b2 = new JButton("Save & Add Reported Officer");
		b2.setBounds(30,200,220,30);
		add(b2);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(t1.getText().toString().isEmpty()||t2.getText().toString().isEmpty()||t3.getText().toString().isEmpty()||t4.getText().toString().isEmpty()){
					JOptionPane.showMessageDialog(null,"Please Fill all the Fields","ERROR",JOptionPane.ERROR_MESSAGE);
				}else{
				try{
					FileWriter fw1 = new FileWriter("alldetails.txt",true);
					fw1.write(t1.getText()+","+t2.getText()+","+t3.getText()+","+t4.getText()+"\n");
					fw1.close();
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f,"Saved Successfully!!");
					dispose();
				}catch(Exception ex){}
				dispose();
					Report r = new Report();
					r.setBounds(400,60,250,250);
				r.setVisible(true);}
			}
		});
	}
}
class Report extends JFrame{
	JButton b2;
	JLabel l1,l2,l3;
	JTextField t1,t2,t3;
	Report(){
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Report Details");
		l1 = new JLabel("R_Name");
		l2 = new JLabel("R_Mobile No");
		l3 = new JLabel("R_Type");
		l1.setBounds(20,40,100,30);
		l2.setBounds(20,80,100,30);
		l3.setBounds(20,120,100,30);
		add(l1);
		add(l2);
		add(l3);
		t1 = new JTextField(30);
		t2 = new JTextField(30);
		t3 = new JTextField(30);
		t1.setBounds(130,40,100,30);
		t2.setBounds(130,80,100,30);
		t3.setBounds(130,120,100,30);
		add(t1);
		add(t2);
		add(t3);
		b2 = new JButton("Submit");
		b2.setBounds(60,160,80,30);
		add(b2);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(t1.getText().toString().isEmpty()||t2.getText().toString().isEmpty()||t3.getText().toString().isEmpty()){
					JOptionPane.showMessageDialog(null,"Please Fill all the Fields","ERROR",JOptionPane.ERROR_MESSAGE);
				}else{
				try{
					FileWriter fw1 = new FileWriter("alldetails.txt",true);
					fw1.write(t1.getText()+","+t2.getText()+","+t3.getText()+"\n");
					fw1.close();
					JFrame f = new JFrame();
					JOptionPane.showMessageDialog(f,"Saved Successfully!!");
					dispose();
				}catch(Exception ex){}
				try (BufferedReader reader = new BufferedReader(new FileReader("alldetails.txt"))) {
					String line;
					StringBuilder sb = new StringBuilder();
					while ((line = reader.readLine()) != null) {
						line = line.replaceAll("\\r|\\n",",");
						sb.append(line);
						sb.append(",");}
						FileWriter fw1 = new FileWriter("details.txt",true);
						fw1.write(sb.toString()+"\n");
						fw1.close();
				} catch (Exception ex) {}
				try{File file = new File("alldetails.txt");
						file.delete();}catch (Exception ex) {}
				JFrame f = new JFrame();
				JOptionPane.showMessageDialog(f,"Details Submited");
				dispose();
				Ars a = new Ars();
				a.setBounds(400,60,350,200);
				a.setVisible(true);}
			}
		});
	}
}
class History extends JFrame{
	JTextField t1;
    JButton b1,b2,b3;
    JTable table;
    JPanel p1;
    JLabel l1;
    ArrayList<String[]> data = new ArrayList<>();
	List<Integer> matchingRows;
	DefaultTableModel model;
	JScrollPane sp;
    History(){
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Report History");
        b1 = new JButton("<--Back");
        b1.setBounds(10,20,80,25);
        add(b1);
       b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				dispose();
				Ars a = new Ars();
				a.setBounds(400,60,350,200);
				a.setVisible(true);
			}
		}); 
        p1 = new JPanel();
        p1.setBounds(10,80,1300,550);
        add(p1);
        try {		
            BufferedReader reader = new BufferedReader(new FileReader("details.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] row = line.split(",");
                data.add(row);
            }
            reader.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        String[] columnNames={"Vi_Name","Vi_Mobile No","Vi_Address","S_Name","S_Number","S_Address","Accident Date","V_Owner Name","V_Name","V_No","V_Regd No","V_Insurence ?","P_Name","P_ID","P_Mobile No","P_Location","R_Name","R_Mobile No","R_Type"};
        model = new DefaultTableModel(columnNames,0);
        for (String[] row : data) {
            model.addRow(row);
        }
        table = new JTable(model);
        sp = new JScrollPane(table);
        sp.setPreferredSize(new Dimension(1200, 500));
        sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        p1.add(sp);
        t1 = new JTextField(60);
        t1.setBounds(200,40,200,30);
        add(t1);
        b2 = new JButton("Search");
        b2.setBounds(400,40,100,30);
        add(b2);
        b2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String query = t1.getText().toString().toLowerCase();
				matchingRows = new ArrayList<Integer>();
				table.clearSelection();
                for (int i = 0; i < table.getRowCount(); i++) {
                    for (int j = 0; j < table.getColumnCount(); j++) {
                        String value = table.getValueAt(i, j).toString().toLowerCase();
                        if (value.contains(query)) {							
                            matchingRows.add(i);
							break;
							}
						}
					}
					for (int i : matchingRows) {
						table.addRowSelectionInterval(i, i);}
                    t1.setText("");
            }
        });
		b3 = new JButton("View Insurence Details");
		b3.setBounds(510,40,200,30);
		add(b3);
		b3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				dispose();
				ViewInsurence v = new ViewInsurence();
				v.setBounds(200,60,600,400);
				v.setVisible(true);
			}
		});
    }
}
class ViewInsurence extends JFrame{
	JTextField t1;
    JButton b1,b2;
    JTable table;
    JPanel p1;
    JLabel l1;
    ArrayList<String[]> data = new ArrayList<>();
	List<Integer> matchingRows;
	DefaultTableModel model;
	JScrollPane sp;
    ViewInsurence(){
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("View Insurence Details");
        b1 = new JButton("<--Back");
        b1.setBounds(10,40,80,25);
        add(b1);
       b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				dispose();
				History h = new History();
				h.setBounds(40,50,1300,600);
				h.setVisible(true);
			}
		}); 
        p1 = new JPanel();
        p1.setBounds(10,80,500,300);
        add(p1);
        try {		
            BufferedReader reader = new BufferedReader(new FileReader("Insurencedetails.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] row = line.split(",");
                data.add(row);
            }
            reader.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        String[] columnNames={"IC_Name","IC_Address","Policy_No","IC_Date",};
        model = new DefaultTableModel(columnNames,0);
        for (String[] row : data) {
            model.addRow(row);
        }
        table = new JTable(model);
        sp = new JScrollPane(table);
        sp.setPreferredSize(new Dimension(500, 300));
        sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        p1.add(sp);
        t1 = new JTextField(60);
        t1.setBounds(200,40,200,30);
        add(t1);
        b2 = new JButton("Search");
        b2.setBounds(400,40,100,30);
        add(b2);
        b2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String query = t1.getText().toString().toLowerCase();
				matchingRows = new ArrayList<Integer>();
				table.clearSelection();
                for (int i = 0; i < table.getRowCount(); i++) {
                    for (int j = 0; j < table.getColumnCount(); j++) {
                        String value = table.getValueAt(i, j).toString().toLowerCase();
                        if (value.contains(query)) {							
                            matchingRows.add(i);
							break;
							}
						}
					}
					for (int i : matchingRows) {
						table.addRowSelectionInterval(i, i);}
                    t1.setText("");
            }
        });
    }
}
public class Tars {
	public static void main(String[] args){
		Ars a = new Ars();
		a.setBounds(400,60,350,200);
		a.setVisible(true);
	}
}